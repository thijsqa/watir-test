require 'program_page'
require 'status_code'
require 'watir'
require 'watir-scroll'

World(PageObject::PageFactory)

Given(/^I visit "([^"]*)"$/) do |pageType|
  case pageType
    when "the homepage"
    when "a program page"
      @programPage = ProgramPage.new(@browser)
      @page = @programPage
      @page_type = ProgramPage
    when "a non compliant program page"
      @programPage = NonCompliantProgramPage.new(@browser)
      @page = @programPage
      @page_type = NonCompliantProgramPage
    end

    visit_page(@page_type, :using_params => {:volatile_issue => @volatile_id}); sleep 1
    puts "Navigated to #{@browser.url}"
  end

When(/^I click on "([^"]*)"$/) do |item|
  case item
    when "download button"
      @page.download_button
    end
  case item
    when "every article"
      @page.list_of_articles
    end
  end

Then(/^"([^"]*)" should be available$/) do |item|
  case item
    when "the download page"
      @page.download_info do |item|
      expect(item.download_info.present?).to be_truthy, "Download info should be present, but they are NOT"
    end
  end
end

Then(/^I should see "([^"]*)" main information$/) do |item|
  case item
    when "every articles"
      @page.download_info do |item|
      expect(item.download_info.present?).to be_truthy, "Download info should be present, but they are NOT"
    end
  end
end
