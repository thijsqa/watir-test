@24x7

Feature: Consume all information in a Program Page
In order to find info about my program
As a desktop user
I need to be able to visit a program page and find all information related to it

Background:
Given I visit "a program page"

@24x7
@test
@page_load
Scenario: Program Page main info
  Then I should see a list with at least "1" "top download"
  And I should see "the program" main information

@24x7
@pp
Scenario: Click on a download button
  When I click on "download button"
  Then "the download page" should be available
  And I should see "every articles" main information

@24x7
Scenario: Program Page related articles
  Then I should see a list with at least "1" "related article"
  And I should see "every articles" main information

@24x7
Scenario: Program Page comments
  Then I should see a list with at least "1" "comment"
  And I should see "every comment" main information
