require 'watir'
require 'cucumber'
require 'rspec'
require 'page-object'
require 'parallel_tests'
require 'report_builder'
require 'page-object/page_factory'
require 'fig_newton'


def browser_name
    (ENV['BROWSER'] ||= 'chrome').downcase.to_sym  # allows me to pass browser as a command line argument
end
def host
    (ENV['HOST'] ||= 'chrome_debug:4444').downcase.to_sym  # allows me to set host for testing as argument. defaults to 'local'
end
def platform
    (ENV['PLATFORM'] ||= 'desktop').downcase.to_sym  # allows me to set platform for testing as argument. defaults to 'desktop'
end
def volatile_id
    (ENV['VOLATILE_ID'] ||= 'none').downcase.to_sym  # allows me to set volatile_id for testing as argument. defaults to 'desktop'
end

# Used to locate the Chrome and Firefox drivers when executing the tests in the local machine
# Selenium::WebDriver::Chrome.driver_path="./link_browser_drivers/chromedriver"
# Selenium::WebDriver::Firefox.driver_path="./link_browser_drivers/geckodriver"

Before do
  case volatile_id
    when :none, :""
      # puts "volatile_id NOT provided"
      # FigNewton.load('production.yml')
      @volatile_id = ""
    else
      # puts "volatile_id provided"
      # FigNewton.load('volatile.yml')
      @volatile_id = "http://#{volatile_id}."
  end
  # puts "volatile ID = #{@volatile_id}"

  host_url = "http://#{host}/wd/hub"

  attempts = 0  # has to be outside the begin/rescue to avoid infinite loop

  begin
    case host
      when :'localhost:4444'
        @browser = Watir::Browser.new browser_name
      else
        driver = Selenium::WebDriver.for browser_name, url: host_url
        @browser = Watir::Browser.new(driver)
    end
    @browser.window.resize_to(1280, 1024)

    rescue Net::ReadTimeout => e
      puts "Exception Message: #{ e.message }"
      puts "Exception Backtrace: #{ e.backtrace }"
      if attempts == 0
        attempts += 1
        retry
      else
        # raise
        puts "Test not executed due to Net::ReadTimeout error"
      end
  end
end

After do |scenario|
   if scenario.failed?  # the classic take a screenshot if scenario fails block.
     Dir::mkdir('screenshots') if not File.directory?('screenshots')
     screenshot = "./screenshots/FAILED_#{scenario.name.gsub(' ','_').gsub(/[^0-9A-Za-z_]/, '')}.png"
     @browser.driver.save_screenshot(screenshot)
     embed screenshot, 'image/png'
   end
  @browser.close
end
