require 'page-object'
require 'download_page_sections'
require 'popups'

class DownloadPage
  include PageObject

  params = {:volatile_issue => ''}
  page_url "<%=params[:volatile_issue]%>winzip.#{FigNewton.base_url}/download?countryCode=US&fex=false"

  page_section(:cookies_message, CookiesMessage, :class => 'optanon-alert-box-bg')
  page_section(:download_section, DownloadSection, :class => 'app-download-info__main-content')

  meta(:download_button, css: 'meta[data-auto="download-button"]')

  def download_page
    self.download_button.scroll.to :center
    self.download_button.click
  end
end
