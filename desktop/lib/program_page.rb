require 'page-object'
require 'program_page_sections'
require 'popups'
require 'download_page_sections'

class ProgramPage
  include PageObject

  params = {:volatile_issue => ''}
  page_url "<%=params[:volatile_issue]%>winzip.#{FigNewton.base_url}/?countryCode=US&fex=false"

  page_section(:cookies_message, CookiesMessage, :class => 'optanon-alert-box-bg')
  page_section(:download_section, DownloadSection, :class => 'app-download-info__main-content')
  page_section(:list_of_articles, ArticlesList, :class => 'card-article')

  link(:download_button, :xpath => "//div[@class='app-download-btn']/a")
  #link(:download_button, :css => 'meta[data-auto="download-button"]')

  def click_download_button
    self.download_button.scroll.to :center
    self.download_button.click
  end

  def download_info
    self.download_section
  end

end
