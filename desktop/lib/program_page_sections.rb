require 'page-object'
require 'program_page'

class ArticlesList
  include PageObject

  div(:image, :class => 'card-article__image')
  h3(:title, :xpath => ".//*[@class='card-article__headline']")
  link(:read_more, :xpath => ".//span[@class='card-article__link']")
end
