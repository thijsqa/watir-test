require 'net/https'
require 'uri'

def check_status_code(url)
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = (uri.scheme == "https")
  request = Net::HTTP::Get.new(uri.request_uri)
  res = http.request(request)
  puts "status code = #{res.code}"
  return res.code
end
