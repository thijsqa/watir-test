require 'page-object'

class DownloadSection
  include PageObject

  div(:download_info, :xpath => "//div[contains(@class, 'app-download-info__main-content')]")

end

class ArticlesList
  include PageObject

  div(:image, :class => 'card-article__image')
  h3(:title, :xpath => ".//*[@class='card-article__headline']")
  link(:read_more, :xpath => ".//span[@class='card-article__link']")
end
