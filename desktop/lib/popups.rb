require 'page-object'

class CookiesMessage
  include PageObject

  link(:accept, :class => 'optanon-allow-all')

end
