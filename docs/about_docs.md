# About this documentation

To generate this documentation you need the following files in your docs directory:

`atlas-qa/docs/`

- `Dockerfile`: Necessary to build the atlas-qa-docs Docker image hosted in the private Softonic's Registry account.

- `docker-compose.yml`: Configuration for the docs service.

- `mkdocs.yml`: Several configurations specific to the documentation, as site information, repository, mkdocs extensions and the pages structure.

- `Jenkinsfile`: Configuration for project deployment via Jenkins.


## How to edit the documentation in local

This documentation can be built in your local machine where you can edit it and see the result in your localhost.

In `atlas-qa/docs/`

```
docker build -t softonic/atlas-qa-docs .
docker run --rm --volume="$PWD:/docs/docs" --volume="$PWD/mkdocs.yml:/docs/mkdocs.yml" -p 80:8000 softonic/atlas-qa-docs
```

Go to `http://localhost` to see the documentation. As we run it creating volumes for the docs folder and the mkdocs.yml file, any change we do will be reflected in the site when we refresh.


## How to deploy the documentation

In Jenkins we have the [Atlas QA Documentation job](https://jenkins.softonic.one/job/Atlas%20QA%20Docs/).

This is a Multibranch pipeline project configured to use the `docs/Jenkinsfile`

![jenkinsfile_docs.png](img/jenkinsfile_docs.png)

It's defined in this Jenkinsfile to create a new version of the documentation in the Softonic's registry account and then deploy it to swarm-staging. The result of the deploy is notified in the specified Slack channel.

To deploy the documentation, we need to go to the [Atlas QA Documentation job](https://jenkins.softonic.one/job/Atlas%20QA%20Docs/) and click on `Scan Multibranch Pipeline Now`

![deply_docs.png](img/deploy_docs.png)


## How to see the documentation

The host for the documentation is configured in the `site_url` filed in the `docs/mkdocsk.yml` file and we also need to make it explicit in the network configuration of the `docs/docker-compose.yml` file.

The host structure must always be `project_name.docs.softonic.one`, in this case:

`atlas-qa.docs.softonic.one`

So once the project is deployed just go to `http://atlas-qa.docs.softonic.one/`
