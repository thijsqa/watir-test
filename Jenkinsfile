pipeline {
    agent { node { label 'swarm-ci' } }

    parameters {
      choice(name: 'ENVIRONMENT', choices: 'prod\nvol', description: 'Sets the environment where the tests will run')
      string(name: 'VOLATILE_ID', defaultValue: 'at-', description: """Only required if environment is set to volatile. \
      Sets the volatile_ID value. Expected format is at-123 (Jira issue ID).""")
      choice(name: 'PLATFORM', choices: 'desktop\nandroid\niphone', description: 'Sets the platform where the tests will run')
      choice(name: 'BROWSER', choices: 'chrome\nfirefox', description: 'Sets the browser where the tests will run')
      string(name: 'TESTS_GROUP', defaultValue: '@', description: """Sets the test group value tags. \
      If no value is given, all the tests will be executed""")
      string(name: 'THREADS', defaultValue: '25', description: 'Sets the number of parallel executions')
      string(name: 'RETRIES', defaultValue: '0', description: 'Sets the number of retries for a failing test')
      string(name: 'SLACK_CHANNEL', defaultValue: '#qa-notifications', description: 'Sets the Clack channel where the tests execution \
      results will be sent')
      choice(name: 'GRID', choices: 'grid_us\ngrid_eu', description: 'Sets the Grid where the tests will execute')
    }

    triggers {
      parameterizedCron("""
        30 8-23/1 * * * %ENVIRONMENT=prod;PLATFORM=desktop;BROWSER=chrome;TESTS_GROUP=@24x7;THREADS=25;RETRIES=1;SLACK_CHANNEL=#atlas-notifications;GRID=grid_us;
        30 0-5/1 * * * %ENVIRONMENT=prod;PLATFORM=desktop;BROWSER=chrome;TESTS_GROUP=@24x7;THREADS=25;RETRIES=1;SLACK_CHANNEL=#atlas-notifications;GRID=grid_us;
        35 8-23/1 * * * %ENVIRONMENT=prod;PLATFORM=android;BROWSER=chrome;TESTS_GROUP=@24x7;THREADS=25;RETRIES=1;SLACK_CHANNEL=#atlas-notifications;GRID=grid_us;
        35 0-5/1 * * * %ENVIRONMENT=prod;PLATFORM=android;BROWSER=chrome;TESTS_GROUP=@24x7;THREADS=25;RETRIES=1;SLACK_CHANNEL=#atlas-notifications;GRID=grid_us;
      """)
    }

    options {
      buildDiscarder logRotator(artifactDaysToKeepStr: '15', artifactNumToKeepStr: '', daysToKeepStr: '15', numToKeepStr: '')
    }

    environment {
      SLACK_TOKEN = credentials("slack_token")
      SLACK_ICON_SUCCESS = ":white_check_mark:"
      SLACK_ICON_FAIL = ":x:"
      SLACKSEND_VERSION = "1.1.0"

      PROJECT_NAME = "atlas QA"
      PROD_URL = "https://solutions.softonic.com"
      VOL_URL = "http://${params.VOLATILE_ID}.solutions.softonic.one/"
      REPORT_URL = "${env.BUILD_URL}Solutions_20tests_20report/"
      REGION_US = "US West Oregon"
      REGION_EU = "EU Frankfurt"
      GRID_US = "http://35.166.39.78:4444/grid/console"
      GRID_EU = "http://35.158.65.233:4444/grid/console"
    }

    stages {
        stage("Prepare") {
            when {
                expression { BRANCH_NAME ==~ /master/ }
            }

            steps {
                script {
                  env_name = (params.ENVIRONMENT == 'prod') ? 'Production' : "Volatile ${params.VOLATILE_ID}"
                  env_url = (params.ENVIRONMENT == 'prod') ? env.PROD_URL : env.VOL_URL
                  env_region = (params.GRID == 'grid_us') ? env.REGION_US : env.REGION_EU
                  env_grid_url = (params.GRID == 'grid_us') ? env.GRID_US : env.GRID_EU
                  sh "docker-compose -f docker-compose.lite.yml pull"
                  env.DEPLOY_VERSION = sh (returnStdout: true, script: """
                    curl -I https://en.softonic.com/solutions 2>&1 | grep x-version | cut -d' ' -f2
                  """).trim()
                  env.DEPLOY_VERSION_URL = "https://bitbucket.org/softonic-development/atlas-mvp/commits/tag/v${env.DEPLOY_VERSION}"
                }
            }
        }
        stage("Run tests and build report") {
            when {
                expression { BRANCH_NAME ==~ /master/ }
            }

            steps {
                script {
                  if (params.ENVIRONMENT == 'prod'){
                    sh """
                      docker-compose -f docker-compose.lite.yml run --rm tests -e prod -p ${params.PLATFORM} -s ${params.GRID} \
                      -b ${params.BROWSER} -g ${params.TESTS_GROUP} -t ${params.THREADS} -r ${params.RETRIES} -d ${BUILD_ID} \
                      -v ${env.DEPLOY_VERSION}
                    """
                  }
                  else{
                    sh """
                      docker-compose -f docker-compose.lite.yml run --rm tests -e vol -i ${params.VOLATILE_ID} -p ${params.PLATFORM} \
                      -s ${params.GRID} -b ${params.BROWSER} -g ${params.TESTS_GROUP} -t ${params.THREADS} -r ${params.RETRIES} -d ${BUILD_ID} \
                      -v ${env.DEPLOY_VERSION}
                    """
                  }
                }
            }

            post {
                always {
                  script {
                    if (params.ENVIRONMENT == 'prod'){
                      publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true,  \
                      reportDir: "reports/${params.PLATFORM}/production/${BUILD_ID}", reportFiles: 'tests_report.html',  \
                      reportName: 'Solutions tests report', reportTitles: ''])
                    }
                    else{
                      publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true,  \
                      reportDir: "reports/${params.PLATFORM}/volatile/${params.VOLATILE_ID}/${BUILD_ID}", reportFiles: 'tests_report.html', \
                      reportName: 'Solutions tests report', reportTitles: ''])
                    }
                  }
                }
                success {
                  script {
                    if (params.ENVIRONMENT == 'prod'){
                        sh """
                              docker run --rm softonic/slacksend:${SLACKSEND_VERSION} \
                                -t '${env.SLACK_TOKEN}' \
                                -c '${env.SLACK_CHANNEL}' \
                                -u '${env.PROJECT_NAME}' \
                                -i '${SLACK_ICON_SUCCESS}' \
                                -s 'SUCCESS' \
                                -z '*<${env_url}|${env_name}>* *${params.PLATFORM}* tests passed\n Executed in <${env_grid_url}|${env_region}>\n <${env.REPORT_URL}|Check Report> for version <${env.DEPLOY_VERSION_URL}|${env.DEPLOY_VERSION}> ' \
                          """
                    }
                    else{
                      sh """
                            docker run --rm softonic/slacksend:${SLACKSEND_VERSION} \
                              -t '${env.SLACK_TOKEN}' \
                              -c '${env.SLACK_CHANNEL}' \
                              -u '${env.PROJECT_NAME}' \
                              -i '${SLACK_ICON_SUCCESS}' \
                              -s 'SUCCESS' \
                              -z '*<${env_url}|${env_name}>* *${params.PLATFORM}* tests passed\n Executed in <${env_grid_url}|${env_region}>\n <${env.REPORT_URL}|Check Report> ' \
                        """
                    }
                  }
                }
                failure {
                  script {
                    if (params.ENVIRONMENT == 'prod'){
                        sh """
                              docker run --rm softonic/slacksend:${SLACKSEND_VERSION} \
                              -t '${env.SLACK_TOKEN}' \
                              -c '${env.SLACK_CHANNEL}' \
                              -u '${env.PROJECT_NAME}' \
                              -i '${SLACK_ICON_FAIL}' \
                              -s 'FAILED' \
                              -z '*<${env_url}|${env_name}>* *${params.PLATFORM}* tests failed\n Executed in <${env_grid_url}|${env_region}>\n <${env.REPORT_URL}|Check Report> for version <${env.DEPLOY_VERSION_URL}|${env.DEPLOY_VERSION}> ' \
                          """
                     }
                     else{
                         sh """
                               docker run --rm softonic/slacksend:${SLACKSEND_VERSION} \
                               -t '${env.SLACK_TOKEN}' \
                               -c '${env.SLACK_CHANNEL}' \
                               -u '${env.PROJECT_NAME}' \
                               -i '${SLACK_ICON_FAIL}' \
                               -s 'FAILED' \
                               -z '*<${env_url}|${env_name}>* *${params.PLATFORM}* tests failed\n Executed in <${env_grid_url}|${env_region}>\n <${env.REPORT_URL}|Check Report> ' \
                           """
                     }
                  }
                }
            }
        }
    }
}
