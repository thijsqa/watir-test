#!/bin/bash

usage="$(basename "$0") -- Script that executes the automated tests according to several arguments

which are:
    -e  [MANDATORY] Sets the environment value. Available options: {prod, vol, dev}
                   If environment is set to volatile, then a volatile_id value needs to be provided
    -i  [OPTIONAL] Sets the volatile_ID value. Expected format is {at-123 (Jira issue ID)}
    -p  [OPTIONAL] Sets the profile value according to the platform value. Available options: {desktop, android, iphone}. Default = desktop
    -s  [OPTIONAL] Sets the Selenium host value. Available options: {chrome_debug, firefox_debug, grid, grid_jenkins, local}. Default = chrome_debug
                   If the host is set to chrome_debug, then the browser is automatically set to chrome
                   If the host is set to firefox_debug, then the browser is automatically set to firefox
                   If the host is set to grid or grid_jenkins, then you can set the browser to be chrome or firefox
                   If the host is set to local, then you can set the browser to be chrome or firefox and you need to have all the ruby gems of the Gemfile installed in your machine
    -b  [OPTIONAL] Sets the browser where the tests will run. Available options: {chrome, firefox}. Default = chrome
    -t  [OPTIONAL] Sets the number of parallel executions. Default = 1
    -r  [OPTIONAL] Sets the number of retries for a failing test. Default = 0
    -d  [OPTIONAL] Sets the tests report directory. Only required when the execution is triggered from a Jenkins job. The value is the build id.
                   If the execution is triggered in local, the directory is the timestamp.
                   If the execution is triggered from a Jenkins job the directory is the job Build ID.
    -v  [OPTIONAL] Sets the project verion tag. Applies only for execution triggered from a Jenkins job using the tests Jenkinsfile.
    -g  [OPTIONAL] Sets the test group value tags. Some available options: {@all, @pp, @sp, @login, @ugc}.
                   If no argument is given, all the tests will be executed.
                   The tags are placed in the feature files, tagging the scenarios. They always begin with '@'.
                   FORMAT: Only execute the features or scenarios with tags matching TAG_EXPRESSION.
                           Scenarios inherit tags declared on the Feature level.
                           The simplest TAG_EXPRESSION is simply a tag. Example: -t @dev.
                           To represent boolean NOT preceed the tag with 'not '. Example: -t 'not @dev'.
                           A tag expression can have several tags separated by an or which represents
                           logical OR. Example: -t '@dev or @wip'.
                           A tag expression can have several tags separated by an and which represents
                           logical AND. Example: -t '@dev and @wip'.
                           The -t option can be specified several times, and this also represents logical AND.
                           Example: -t '@foo or not @bar' -t @zap. This represents the boolean expression (@foo || !@bar) && @zap.
                           Beware that if you want to use several negative tags to exclude several tags
                           you have to use logical AND: -t 'not @fixme and not @buggy'.

other useful options:
    -h  show this help text"

bold=$(tput bold)
normal=$(tput sgr0)

if [ $# -eq 0 ]; then
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
  printf "${bold}No arguments provided${normal}, at least the ${bold}environment ${normal}needs to be set. Available options are prod, vol or dev.\n"
  printf "Check the optional execution parameters and their ${bold}available values with -h${normal}\n"
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
  exit 1
fi

# Assign the options provided with the expected arguments
# The first colon (:) disables verbose error handling
# The colons at the right of each option mean that the options requires an argument.
while getopts :e:p:g:i:s:b:t:r:d:v:h option
  do
    case "${option}"
      in
      h) echo "$usage"
         exit ;;
      e) ENVIRONMENT=${OPTARG};;
      i) VOLATILE_ID=${OPTARG};;
      p) PLATFORM=${OPTARG};;
      s) HOST=${OPTARG};;
      b) BROWSER=${OPTARG};;
      t) THREADS=${OPTARG};;
      r) RETRIES=${OPTARG};;
      g) GROUP=${OPTARG};;
      d) JENKINS_REPORTS_DIR=${OPTARG};;
      v) VERSION_TAG=${OPTARG};;
      \?) echo "Invalid option: -$OPTARG"
          exit 1 ;;
      :) echo "Option -$OPTARG requires an argument."
         exit 1
  esac
done

# Set the timestamp format for tests reports
TIMESTAMP=`date +%d_%m-%H_%M_%S`

# If a Jenkins reports directory is provided, the build number will be set as the reports directory
# If a Jenkins reports directory is not provided, the timestamp will be used to set it as reports directory
if [ -n "$JENKINS_REPORTS_DIR" ]; then
  dir_id=$JENKINS_REPORTS_DIR
else
  dir_id=$TIMESTAMP
fi

# If tags are provided, they will be used in the cucumber/parallel_cucumber command execution
# If no tags are provided, all the tests will be executed
if [ -n "$GROUP" ]; then
  TAGS="-t '$GROUP'"
else
  GROUP="All Tests"
fi

# If threads are provided, they will be used in the cucumber/parallel_cucumber command execution
# If no threads are provided then it's set to its default value (1)
if [ -z "$THREADS" ]; then
  THREADS=1
fi

# If number of retries are provided, they will be used in the cucumber/parallel_cucumber command execution
# If no number of retries are provided then it's set to its default value (0)
if [ -z "$RETRIES" ]; then
  RETRIES="0"
fi

# Available browsers: chrome, firefox
# If no browser is provided then it's set to its default value (chrome)
if [ -n "$BROWSER" ]; then
  case "$BROWSER" in
  chrome|firefox)
      BROWSER_ENV="BROWSER=$BROWSER";;
  *)  echo "Browser needs to be either chrome or firefox"
      exit 1 ;;
  esac
else
  BROWSER="chrome"
  BROWSER_ENV="BROWSER=chrome"
fi

# Available hosts: local, grid, chrome_debug and firefox_debug
# If no host is provided then it's set to its default value (chrome_debug)
if [ -n "$HOST" ]; then
  case "$HOST" in
    local)
      selenium_host="localhost:4444";;
    chrome_debug)
      selenium_host="chrome_debug:4444"
      BROWSER="chrome"
      BROWSER_ENV="BROWSER=$BROWSER"
      echo "Browser has been set to Chrome automatically because the Selenium Host is chrome_debug, if you provided a value for the browser it will be overwritten";;
    firefox_debug)
      selenium_host="firefox_debug:4444"
      BROWSER="firefox"
      BROWSER_ENV="BROWSER=$BROWSER"
      echo "Browser has been set to Firefox automatically because the Selenium Host is firefox_debug, if you provided a value for the browser it will be overwritten";;
    grid)
      selenium_host="35.158.65.233:4444";;
    grid_jenkins)
      selenium_host="172.31.10.108:4444";;
    grid_us)
      selenium_host="10.139.2.147:4444";;

    *)  echo "Host needs to be either chrome_debug, firefox_debug, grid, grid_jenkins, grid_us or local"
        exit 1 ;;
  esac
else
  selenium_host="chrome_debug:4444"
  HOST="chrome_debug"
  BROWSER="chrome"
  BROWSER_ENV="BROWSER=$BROWSER"
  echo "Browser has been set to Chrome automatically because the Selenium Host is chrome_debug, if you provided a value for the browser it will be overwritten"
fi
HOST_ENV="HOST=$selenium_host"

# Available platforms: desktop, android and iphone
# If no platform is provided then it's set to its default value (desktop)
if [ -n "$PLATFORM" ]; then
  case "$PLATFORM" in
  desktop)
      tests_dir="desktop"
      platform_dir=reports/desktop
      PLATFORM_ENV="PLATFORM=$PLATFORM";;
  android|iphone)
      tests_dir="mobile"
      platform_dir=reports/$PLATFORM
      PLATFORM_ENV="PLATFORM=$PLATFORM";;
  *)  echo "Platform needs to be either desktop, android or iphone"
      exit 1 ;;
  esac
else
  PLATFORM="desktop"
  tests_dir="desktop"
  platform_dir=reports/desktop
  PLATFORM_ENV="PLATFORM=$PLATFORM"
fi

# Available environments: production, volatile and devel
# If no environment is provided then it's set to its default value (production)
if [ -n "$ENVIRONMENT" ]; then
  case "$ENVIRONMENT" in
  prod)
      environment_dir=$platform_dir/production
      # If a Version Tag is provided, it will be printed in the tests report
      if [ -n "$VERSION_TAG" ]; then
        version_tag=$VERSION_TAG
      else
        version_tag="N/A"
      fi;;
  dev)
      environment_dir=$platform_dir/devel
      version_tag="N/A";;
  vol)
      if [ -n "$VOLATILE_ID" ]; then
        environment_dir=$platform_dir/volatile/$VOLATILE_ID
        # Needed for tests execution. VOLATILE_ID_ENV is a required argument in case the tests execution is in a Volatile environment
        VOLATILE_ID_ENV="VOLATILE_ID="$VOLATILE_ID
        version_tag="N/A"
      else
        echo "If environment is set to volatile you need to provide a volatile id"
        exit 1
      fi;;
  *)  echo "Environment needs to be either prod, vol or dev"
      exit 1 ;;
  esac
fi
mkdir -p $environment_dir

# Report directory creation. Created in the corresponding environemnt directory
mkdir -p $environment_dir/$dir_id
reports_dir=$environment_dir/$dir_id

# export $REPORT_DIR=reports_dir

# Loop until Selenium server up and running. Maximum time to wait is set in selenium_timeout variable. Only for chrome_debug and firefox_debug
if [ $HOST == "chrome_debug" ] || [ $HOST == "firefox_debug" ]; then
  selenium_timeout=5
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
  printf "Waiting for Selenium host ${bold}$selenium_host to be available${normal}\n"
  until $(curl --output /dev/null --silent --head --fail http://$selenium_host/wd/hub); do
      printf '.'
      if [ $selenium_timeout -eq 0 ]; then
        printf "\nWait for Selenium host timeout reached. ${bold}The tests can't be executed because the selenium host is not available${normal}\n"
        printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
        exit 1
      fi
      sleep 1
      let "selenium_timeout--"
  done
fi

# Tests execution parameters information
printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
printf "${bold}Executing the tests ${normal}according to the following parameters:\n
ENVIRONMENT: ${bold}$ENVIRONMENT${normal}\n"
if [ -n "$VOLATILE_ID" ]; then
  printf "VOLATILE_ID: ${bold}$VOLATILE_ID${normal}\n"
fi
printf "PLATFORM: ${bold}$PLATFORM${normal}
GROUP: ${bold}$GROUP${normal}
HOST: ${bold}$HOST${normal}
BROWSER: ${bold}$BROWSER${normal}
THREADS: ${bold}$THREADS${normal}
RETRIES: ${bold}$RETRIES${normal}
JENKINS_REPORTS_DIR: ${bold}$JENKINS_REPORTS_DIR${normal}\n"
printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -

# Select the directory according to test execution. Desktop for platform == desktop and Mobile for platform == android|iphone
# Execute Cucumber tests in parallel. -n 4 means it will run using 4 threads, set the number according to your needs.
cd $tests_dir

parallel_cucumber -n $THREADS features --group-by scenarios --first-is-1 -o "$VOLATILE_ID_ENV $PLATFORM_ENV $HOST_ENV $BROWSER_ENV \
$TAGS -p $ENVIRONMENT -p reports --retry $RETRIES" 2> tests_execution_result

ter="$(cat tests_execution_result)"
echo "Tests execution result = $ter"

cd ..

# Variables related to tests report information
LANG=C
date=$(date '+%a %d %b %Y - %H.%M.%S');
REPORT_TITLE="Tests results in $PLATFORM $ENVIRONMENT $VOLATILE_ID &nbspfor the tests group $GROUP"
REPORT_NAME="tests_report"
REPORT_INFO="Date:"$date", Browser:$BROWSER, Environment:$ENVIRONMENT $VOLATILE_ID, Platform:$PLATFORM, Group:$GROUP, Host:$HOST, Project Version:$version_tag"

# Check if there are .json files in the reports folder
if ls reports/*.json &>/dev/null; then
  # parallel_cucumber generates a report json file for each thread
  # Move all the threads reports to the timestamped folder in the corresponding environment directory
  mv reports/*.json $reports_dir
  #Generate a report with the result of all the thredas json reports
  sleep 1 && report_builder -s $reports_dir -o "$reports_dir/$REPORT_NAME" --title "$REPORT_TITLE" --info "$REPORT_INFO"
  # Remove the threads json files once the HTML report is build_report
  echo "Find the report in ${bold}$reports_dir${normal}"
  rm $reports_dir/*.json

 else
   printf "${bold}Didn't find any report files in reports folder, there might have been a problem with the tests execution${normal}\n"
   printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
fi

# Checks if the Cucumber tests passed or failed and sets the exit code accordingly
if [ -n "$ter" ]
  then
    echo "Cucumbers failed so build failed"
    exit 1
  else
    echo "Cucumbers passed so build succeeded"
fi
