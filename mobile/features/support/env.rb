require 'watir'
require 'cucumber'
require 'rspec'
require 'page-object'
require 'parallel_tests'
require 'report_builder'
require 'page-object/page_factory'
require 'fig_newton'


def browser_name
    (ENV['BROWSER'] ||= 'chrome').downcase.to_sym  # allows me to pass browser as a command line argument
end
def host
    (ENV['HOST'] ||= 'local').downcase.to_sym  # allows me to set host for testing as argument. defaults to 'local'
end
def platform
    (ENV['PLATFORM'] ||= 'android').downcase.to_sym  # allows me to set platform for testing as argument. defaults to 'desktop'
end
def volatile_id
    (ENV['VOLATILE_ID'] ||= 'none').downcase.to_sym  # allows me to set volatile_id for testing as argument. defaults to 'desktop'
end

# Used to locate the Chrome and Firefox drivers when executing the tests in the local machine
# Selenium::WebDriver::Chrome.driver_path="./link_browser_drivers/chromedriver"
# Selenium::WebDriver::Firefox.driver_path="./link_browser_drivers/geckodriver"

Before do

  case volatile_id
  when :none, :""
      # FigNewton.load('production.yml')
      @volatile_id = ""
    else
      # FigNewton.load('volatile.yml')
      @volatile_id = "http://#{volatile_id}."
  end
  # puts "volatile ID = #{@volatile_id}"

  host_url = "http://#{host}/wd/hub"

  case browser_name
    when :chrome
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument("--disable-notifications")
      options.add_argument("disable-infobars");
      case platform
        when :android
          options.add_emulation(device_name: 'Nexus 5')
        when :iphone
          options.add_emulation(device_name: 'iPhone 6')
      end
    when :firefox
      profile = Selenium::WebDriver::Firefox::Profile.new
      user_agent_android = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1'
      user_agent_iphone = 'Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36'
      profile['dom.webnotifications.enabled'] = false

      case platform
        when :android
          profile['general.useragent.override'] = user_agent_android
        when :iphone
          profile['general.useragent.override'] = user_agent_iphone
      end
  end

  # host_url = 'http://localhost:4444/wd/hub' if host == :local
  # host_url = 'http://35.158.65.233:4444/wd/hub' if host == :grid_qa
  # host_url = "http://#{host}:4444/wd/hub" if host == :chrome_debug || host == :firefox_debug
  # puts "Host URL = #{host_url}"

  attempts = 0  # has to be outside the begin/rescue to avoid infinite loop

  begin
    case host
      when :'localhost:4444'
        case browser_name
          when :chrome
            @browser = Watir::Browser.new browser_name, options: options
          when :firefox
            @browser = Watir::Browser.new browser_name, profile: profile
        end
      else
        case browser_name
          when :chrome
            driver = Selenium::WebDriver.for :chrome, options: options, url: host_url
            @browser = Watir::Browser.new(driver)
          when :firefox
            options = Selenium::WebDriver::Firefox::Options.new(profile: profile)
            driver = Selenium::WebDriver.for :firefox, options: options, url: host_url
            @browser = Watir::Browser.new(driver)
        end
    end
    @browser.window.resize_to(360, 800)

    rescue Net::ReadTimeout => e
        puts "Exception Message: #{ e.message }"
        puts "Exception Backtrace: #{ e.backtrace }"
        if attempts == 0
          attempts += 1
          retry
        else
          # raise
          puts "Test not executed due to Net::ReadTimeout error"
        end
  end
end

After do |scenario|
   if scenario.failed?  # the classic take a screenshot if scenario fails block.
     Dir::mkdir('screenshots') if not File.directory?('screenshots')
     screenshot = "./screenshots/FAILED_#{scenario.name.gsub(' ','_').gsub(/[^0-9A-Za-z_]/, '')}.png"
     @browser.driver.save_screenshot(screenshot)
     embed screenshot, 'image/png'
   end
  @browser.close
end
