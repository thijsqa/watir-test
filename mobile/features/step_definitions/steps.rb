require 'problem_page'
require 'problem_page_nc'
require 'solution_page_nc'
require 'alternatives_page_nc'
require 'solution_page'
require 'alternatives_page'
require 'login_page'
require 'gdpr_compliant_page'
require 'credentials'
require 'status_code'
require 'watir'
require 'watir-scroll'

World(PageObject::PageFactory)

Given(/^I visit "([^"]*)"$/) do |item|
  case item
  when 'a problem page'
    @problem_page = ProblemPage.new(@browser)
    @page = @problem_page
    @page_type = ProblemPage
  when 'a solution page'
    @solution_page = SolutionPage.new(@browser)
    @page = @solution_page
    @page_type = SolutionPage
  when "an alternatives page"
    @alternativesPage = AlternativesPage.new(@browser)
    @page = @alternativesPage
    @page_type = AlternativesPage
  when "a non compliant problem page"
    @problemPage = NonCompliantProblemPage.new(@browser)
    @page = @problemPage
    @page_type = NonCompliantProblemPage
  when "a non compliant solution page"
    @solutionPage = NonCompliantSolutionPage.new(@browser)
    @page = @solutionPage
    @page_type = NonCompliantSolutionPage
  when "a non compliant alternatives page"
    @alternativesPage = NonCompliantAlternativesPage.new(@browser)
    @page = @alternativesPage
    @page_type = NonCompliantAlternativesPage
  when "a GDPR compliant page"
    @gdprPage = GdprCompliantPage.new(@browser)
    @page = @gdprPage
    @page_type = GdprCompliantPage
  end
  visit_page(@page_type, using_params: { volatile_issue: @volatile_id }); sleep 1
  # @browser.cookies.add('modal_shown', 'true')
  # if @page.push_notification.close_element.present?
  #   @page.push_notification.close; sleep 1
  # end
  puts "Navigated to #{@browser.url}"
end

When("I accept the cookies message") do
  # if @page.cookies_message.accept_element.present?
  @page.cookies_message.accept_element.wait_until_present
  @page.cookies_message.accept_element.fire_event('onclick')
  sleep(1)
  # end
end

When(/^I click on "([^"]*)"$/) do |item|
  case item
  when 'the first platform'
    @page.click_first_platform_in_first_platform_dropdown
  when 'the sign in button'
    @page.header.sign_in
  when 'the add a solution button'
    @page.add_solution_button
  when 'the first platform of the action button'
    @page.click_first_platform_in_action_button
  when 'the ask a question button'
    @page.header.ask_question_button_element.wait_until_present
    @page.header.ask_question_button
  when 'the first leave a comment button'
    @page.first_leave_comment_button_element.scroll.to :center
    @page.first_leave_comment_button_element.click
  when "the sponsored solution action button"
    @page.list_of_sponsored_solutions[0].scroll.to :top
    # @page.list_of_sponsored_solutions[0].action
    @page.list_of_sponsored_solutions[0].action_element.fire_event('click')
  when "the first alternative solution"
    @url = @page.get_first_alternative_solution_url
    @page.click_first_alternative_solution
  end
end

When(/^I log in using a "([^"]*)" account$/) do |item|
  case item
  when 'FB'
    @login_page = LoginPage.new(@browser)
    @login_page.login_Fb
  when 'Google'
    @login_page = LoginPage.new(@browser)
    @login_page.login_Google
  end
end

When(/^I fill in the "([^"]*)" form$/) do |item|
  case item
  when 'solution'
    @page.add_solution.fill_in_solution
  when 'comment'
    # Comments forms are a collection where the form for the use case item selected is shown while the rest are hidden.
    # The first use case comment form is the last of the collection, that's why we take the index from the last item in the collection.
    # @first_comment_form_index = @page.leave_comment.length-1
    # @page.leave_comment[@first_comment_form_index].submit_comment
    @page.leave_comment[0].scroll.to :top
    @page.leave_comment[0].submit_comment
  end
end

When('I submit a question') do
  @page.ask_question.submit_question
  # Workaround to submit the question because the Ask button is not clicked
  @browser.send_keys :enter; sleep 2
end

Then(/^I should see a list with at least "([^"]*)" "([^"]*)"$/) do |num_expected_items, item|
  case item
  when 'solutions'
    numActualItems = @page.list_of_solutions.length
  when "alternative solutions"
    numActualItems = @page.list_of_alternative_solutions.length
  when 'use case'
    numActualItems = @page.list_of_use_cases.length
  when "sponsored solution"
    numActualItems = @page.list_of_sponsored_solutions.length
  end
  puts "#{numActualItems} items found in the list"
  expect(numActualItems).to be >= num_expected_items.to_i, "Number of items expected were at least #{num_expected_items}, actual are #{numActualItems}"
end

Then(/^"([^"]*)" should not be present$/) do |item|
  case item
  when 'the cookies message'
    expect(@page.cookies_message.present?).to be_falsey, "The cookies message should not be present, but it is"
  end
end

Then(/^I should see "([^"]*)" main information$/) do |item|
  case item
  when 'every solution'
    @page.list_of_solutions.each_with_index do |item, index|
      list_position = index + 1
      # expect(item.rank_element.present?).to be_truthy, "For each item in the list, the solution title should be present, but it's not present in position #{list_position}"
      expect(item.title_element.present?).to be_truthy, "For each item in the list, the solution title should be present, but it's not present in position #{list_position}"
      expect(item.action_element.exists?).to be_truthy, "For each item in the list, the solution action button should be present, but it's not present in position #{list_position}"
    end
  when 'every alternative solution'
    @page.list_of_alternative_solutions.each_with_index do |item, index|
      list_position = index + 1
      expect(item.title_element.present?).to be_truthy, "For each item in the list, the solution title should be present, but it's not present in position #{list_position}"
      expect(item.license_element.present?).to be_truthy, "For each item in the list, the solution license should be present, but it's not present in position #{list_position}"
      expect(item.rating_element.present?).to be_truthy, "For each item in the list, the solution rating should be present, but it's not present in position #{list_position}"
      expect(item.similar_to_button_element.exists?).to be_truthy, "For each item in the list, the solution similar to button should be present, but it's not present in position #{list_position}"
    end
  when 'the solution'
    expect(@page.title_element.present?).to be_truthy, "The solution title should be present, but it's not"
    expect(@page.action_button_element.present?).to be_truthy, "The solution action button should be present, but it's not present"
  when 'the main alternative solution'
    expect(@page.title_element.present?).to be_truthy, "The solution title should be present, but it's not"
    expect(@page.description_element.present?).to be_truthy, "The solution description should be present, but it's not"
    expect(@page.action_button_element.present?).to be_truthy, "The solution action button should be present, but it's not present"
  when 'every use case'
    @page.list_of_use_cases.each_with_index do |item, index|
      list_position = index + 1
      expect(item.rank_element.present?).to be_truthy, "For each item in the list, the solution title should be present, but it's not present in position #{list_position}"
      expect(item.title_element.present?).to be_truthy, "For each item in the list, the solution title should be present, but it's not present in position #{list_position}"
      expect(item.content_element.present?).to be_truthy, "For each item in the list, the solution more info button should be present, but it's not present in position #{list_position}"
    end
  when 'every sponsored solution'
    @page.list_of_sponsored_solutions.each_with_index do |item, index|
      list_position = index + 1
      expect(item.title_element.present?).to be_truthy, "For each item in the list, the solution title should be present, but it's not present in position #{list_position}"
      expect(item.featured_tag).to match("FEATURED"), "For each item in the list, the solution Featured tag text should be 'Featured', actual is #{item.featured_tag} in position #{list_position}"
      expect(item.description_element.present?).to be_truthy, "For each item in the list, the solution description should be present, but it's not present in position #{list_position}"
      expect(item.action_element.exists?).to be_truthy, "For each item in the list, the solution action button should be present, but it's not present in position #{list_position}"
      expect(item.disclaimer_element.present?).to be_truthy, "For each item in the list, the solution disclaimer symbol should be present, but it's not present in position #{list_position}"
    end
  end
end

Then(/^"([^"]*)" should be available$/) do |item|
  case item
  when 'the clicked solution'
    @url = @page.get_first_platform_url
  when 'the clicked sponsored solution'
    @url = @page.list_of_sponsored_solutions[0].get_sponsored_solution_url
  end
  puts "URL to check = #{@url}"
  status_code = check_status_code(@url)
  expect(status_code.to_i).not_to eq(404), "Status code is #{status_code}, expected ¡s anything different from 404 or 500"
  expect(status_code.to_i).not_to eq(500), "Status code is #{status_code}, expected ¡s anything different from 500 or 500"
end

Then('it should open in a new tab') do
  num_tabs = @browser.windows.size
  expect(num_tabs.to_i).to eq(2), "Number of tabs is #{num_tabs}, expected ¡s 2"
end

Then(/^I should see the user info$/) do
  @login_page.user_element.wait_until_present
  expect(@login_page.user_element.present?).to be_truthy, 'The user avatar should be present, but it is not'
end

Then(/^"([^"]*)" should be successfully submitted$/) do |item|
  case item
  when 'the solution'
    @page.add_solution.success_message_element.wait_until_present
    expect(@page.add_solution.success_message_element.present?).to be_truthy, "The add solution confirmation message should be present, but it's not"
    @page.add_solution.continue_button
    expect(@page.add_solution.present?).to be_falsey, 'The add solution form should not be present, but it is'
  when 'the question'
    @page.ask_question.success_message_element.wait_until_present
    expect(@page.ask_question.success_message_element.present?).to be_truthy, "The ask question confirmation message should be present, but it's not"
  when 'the comment'
    # @page.leave_comment[@first_comment_form_index].success_message_element.wait_until_present
    @page.leave_comment[0].success_message_element.wait_until_present
    expect(@page.leave_comment[0].success_message_element.present?).to be_truthy, "The leave comment confirmation message should be present, but it's not"
    # @page.leave_comment[@first_comment_form_index].continue_button
    # expect(@page.leave_comment[@first_comment_form_index].present?).to be_falsey, "The leave comment form should not be present, but it is"
  end
end

Then(/^the page should be marked as "([^"]*)"$/) do |item|
  case item
  when "compliant"
    compliant_value="true"
    puts "This page is a compliant one, so the compliant flag should be set to #{compliant_value}"
  when "non compliant"
    compliant_value="false"
    puts "This page is a non compliant one, so the compliant flag should be set to #{compliant_value}"
  end
  puts "compliant flag = #{@page.compliant_flag_element.attribute_value('content')}"
  expect(@page.compliant_flag_element.attribute_value('content')).to match("#{compliant_value}"), "The page should be marked as compliant, but it's not"
end


Then(/^I should see the main banners$/) do

  if @page.top_leaderboard_wrapper_element.attribute_value('class').include?("ad--empty") then
    puts "Top leaderboard is empty, no ad displayed"
  elsif @page.top_leaderboard_wrapper_element.attribute_value('class').include?("ad--displayed") then
    puts "Top leaderboard displayed"
    expect(@page.ads_banners[0].top_leaderboard_element.present?).to be_truthy, "The top leaderboard banner should be present, but it's not"
  else
    expect(@page.ads_banners[0].top_leaderboard_element.present?).to be_truthy, "The top leaderboard banner should be present, but it's not"
  end
end

# Then(/^I should see the main banners$/) do
#     # In case no ads are displayed, we try loading the page 3 times before setting the test status as failed
#     counter = 3
#     ads_displayed = false
#     while counter > 0 do
#       puts "Value in top_leaderboard_wrapper class= #{@page.top_leaderboard_wrapper_element.attribute_value('class')}"
#       # If the class attribute of the banner_wrapper elements has "add-empty", it means no ads have been displayed.
#       if @page.top_leaderboard_wrapper_element.attribute_value('class').include?("ad--empty") then
#         visit_page(@page_type, :using_params => {:volatile_issue => @volatile_id}); sleep 1
#         counter = counter - 1
#         puts "No display ads loaded, setting the page load retry counter to = #{counter} and reloading the page"
#       else
#         counter = 0
#         ads_displayed = true
#         puts "Display ads loaded, setting the page load retry counter to = #{counter}"
#       end
#       if counter == 0 then
#         puts "ads displayed? = #{ads_displayed}"
#         puts "Checking banners"
#         expect(@page.ads_banners[0].top_leaderboard_element.present?).to be_truthy, "The top leaderboard banner should be present, but it's not"
#         expect(@page.ads_banners[0].top_leaderboard_element.div.iframe.attribute_value('width')).to match("320"), "The top leaderboard banner width should be 320, but it's not. Actual value is #{@page.ads_banners[0].top_leaderboard_element.div_element.iframe.attribute_value('width')}"
#         expect(@page.ads_banners[0].top_leaderboard_element.div.iframe.attribute_value('height')).to match("50") | match("100"), "The top leaderboard banner height should be 50 or 100, but it's not. Actual value is #{@page.ads_banners[0].top_leaderboard_element.div_element.iframe.attribute_value('height')}"
#       end
#     end
# end
