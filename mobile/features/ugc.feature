@ugc

Feature: User Generated Content actions
In order to contribute to the Solutions community
As a mobile user
I need to be able to perform UGC actions


@24x7
@solution
Scenario: Add a solution
  Given I visit "a problem page"
  When I click on "the add a solution button"
  And I fill in the "solution" form
  And I log in using a "FB" account
  Then "the solution" should be successfully submitted

@24x7
@question
Scenario: Ask a question
  Given I visit "a solution page"
  When I click on "the sign in button"
  And I log in using a "Google" account
  And I click on "the ask a question button"
  And I submit a question
  Then "the question" should be successfully submitted

@24x7
@comment
Scenario: Leave a comment
  Given I visit "a solution page"
  When I click on "the first leave a comment button"
  And I fill in the "comment" form
  And I log in using a "FB" account
  Then "the comment" should be successfully submitted
