@pp

Feature: Consume a Solution in a Problem Page
In order to find solutions to my problems
As a mobile user
I need to be able to search for a problem and find a list of solutions related to it

Background:
Given I visit "a problem page"

@24x7
@page_load
Scenario: Problem Page main info
  Then I should see a list with at least "1" "solutions"
  And I should see "every solution" main information

@24x7
@test
Scenario: Click on a solution with several platforms
  When I click on "the first platform"
  Then "the clicked solution" should be available
  And it should open in a new tab

@24x7
@sponsored_solution @pp_ads @ads
Scenario: Problem Page sponsored solutions
  Then I should see a list with at least "1" "sponsored solution"
  And I should see "every sponsored solution" main information

@24x7
@sponsored_solution @pp_ads @ads
Scenario: Click on a sponsored solution
  When I click on "the sponsored solution action button"
  Then "the clicked sponsored solution" should be available
  And it should open in a new tab
