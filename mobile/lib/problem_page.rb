require 'page-object'
require 'problem_page_sections'
require 'header'
require 'login_page'
require 'ask_question'
require 'popups'
require 'ads_banners'

class ProblemPage
  include PageObject

  params = {:volatile_issue => ''}
  page_url "<%=params[:volatile_issue]%>#{FigNewton.base_url}/what-are-the-best-hotel-booking-sites?countryCode=US&fex=false"
  # page_url "<%=params[:volatile_issue]%>#{FigNewton.base_url}/what-are-the-best-apps-to-get-followers-on-instagram"
  # page_url "<%=params[:volatile_issue]%>#{FigNewton.base_url}/what-are-the-best-resources-for-chatting-with-random-people-on-whatsapp"

  page_section(:cookies_message, CookiesMessage, :class => 'optanon-alert-box-wrapper')
  page_section(:push_notification, PushNotification, :id => 'onesignal-popover-dialog')
  page_section(:header, Header, :id => 'header')
  page_sections(:list_of_solutions, SolutionsList, :class => 'card-solution')
  page_sections(:list_of_sponsored_solutions, SponsoredSolutionsList, :class => 'card-sponsored')
  page_section(:add_solution, AddSolution, :xpath => "//div[contains(@class, 'modal-add-solution')]")
  page_section(:ask_question, AskQuestion, :id => 'ask-for-solutions')
  page_sections(:ads_banners, AdsBanners, :class => 'ad-content')

  # link(:close_banner, :title => 'Close Banner')
  meta(:compliant_flag, css: 'meta[name="isGoogleCompliant"]')
  div(:top_leaderboard_wrapper, :id => 'top-leaderboard-first-mobile-wrapper')
  div(:first_platform_dropdown, :xpath => "//div[contains(@class, 'dropdown-platform')]")
  unordered_list(:platform_dropdown_content, :xpath => "//ul[@class='dropdown-platforms__content']")
  link(:add_solution_button, :class => ['button button--small'])


  def click_first_platform_in_first_platform_dropdown
    self.first_platform_dropdown_element.scroll.to :center
    self.first_platform_dropdown_element.click
    self.platform_dropdown_content_element.list_item_element.link.click
  end

  def get_first_platform_url
    self.platform_dropdown_content_element.list_item_element.link_element.attribute_value('href')
  end

end
