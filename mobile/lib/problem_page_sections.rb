require 'page-object'
require 'problem_page'


class SolutionsList
  include PageObject

  div(:rank, :class => 'card-solution__position')
  h2(:title, :class => 'card-solution__title')
  link(:action, :xpath => ".//*[contains(@class, 'js-get-solution')]")
end

class SponsoredSolutionsList
  include PageObject

  h2(:title, :xpath => ".//*[@class='card-sponsored__title']")
  span(:featured_tag, :xpath => ".//*[@class='card-sponsored__position']/span")
  paragraph(:description, :xpath => ".//*[@class='card-sponsored__description']/p")
  link(:action, :xpath => ".//*[@class='card-sponsored__bottom']/a")
  span(:disclaimer, :xpath => ".//*[@class='card-sponsored__position-info']")

  def get_sponsored_solution_url
    self.action_element.attribute_value('href')
  end

end

class AddSolution
  include PageObject

  text_field(:title, :name => 'title')
  text_field(:url, :name => 'url')
  text_area(:comment, :name => 'comment')
  button(:submit, :type => 'submit')
  div(:success_message, :xpath => ".//*[contains(@class, 'form__success-message')]")
  link(:continue_button, :xpath => ".//*[contains(@class, 'js-modal-close')]")

  def fill_in_solution
    self.title = "Test Solution name from QA Automated tests"
    self.url = "http://www.testsolutionurl.qa"
    self.comment = "Test Solution content from QA Automated tests"
    self.submit; sleep 3
  end

end
