require 'page-object'
require 'popups'
require 'ads_banners'

class NonCompliantProblemPage
  include PageObject

  params = {:volatile_issue => ''}
  page_url "<%=params[:volatile_issue]%>#{FigNewton.base_url}/what-are-the-best-alternatives-for-kickass-torrents?countryCode=US&fex=false"

  page_section(:cookies_message, CookiesMessage, :class => 'optanon-alert-box-bg')
  page_sections(:ads_banners, AdsBanners, :class => 'ad-content')
  div(:top_leaderboard_wrapper, :id => 'top-leaderboard-first-mobile-wrapper')
  meta(:compliant_flag, css: 'meta[name="isGoogleCompliant"]')

end
